package src;

import java.util.ArrayList;

public class ImprovedTask5 {
    public static boolean solveSecretLock(int[] array) {
        if (array.length < 6) {
            return false;
        }

        for (int i = 3; i < array.length; i += 3) {
            if (array[i - 3] != array[i] && array[i - 3] != 0 && array[i] != 0) {
                return false;
            }
        }

        for (int i = 4; i < array.length; i += 3) {
            if (array[i - 3] != array[i] && array[i - 3] != 0 && array[i] != 0) {
                return false;
            }
        }

        for (int i = 5; i < array.length; i += 3) {
            if (array[i - 3] != array[i] && array[i - 3] != 0 && array[i] != 0) {
                return false;
            }
        }

        if (getSum(array) > 10) {
            return false;
        }

        var indices = new ArrayList<Integer>();
        for (int i = 0; i <= 2; i++) {
            if (array[i] == 0) {
                array[i] = 1;
                indices.add(i);
            }
        }

        int currentIdx = indices.size() - 1;
        while(getSum(array) == 10) {
            try {
                var idx = indices.get(currentIdx);
                array[idx] = nextValue(array[idx]);

                if (currentIdx != indices.size() - 1) {
                    for (int j = currentIdx + 1; j < indices.size(); j++) {
                        array[indices.get(currentIdx)] = 1;
                    }
                }
            }
            catch (Exception e) {
                if (currentIdx == 0) {
                    return false;
                }

                currentIdx -= 1;
            }
        }

        for (int i = 3; i < array.length; i += 3) {
            array[i] = array[i - 3];
        }

        for (int i = 4; i < array.length; i += 3) {
            array[i] = array[i - 3];
        }

        for (int i = 5; i < array.length; i += 3) {
            array[i] = array[i - 3];
        }

        return true;
    }

    public static int getSum(int[] array) {
        return array[0] + array[1] + array[2];
    }

    private static int nextValue(int n) throws Exception {
        int output;

        int a = n % 10;
        if (a == 6) {
            if (n / 10 == 0) {
                throw new Exception("end");
            }

            output = nextValue(n / 10) * 10;
        } else {
            output = n + 1;
        }

        return output;
    }
}
