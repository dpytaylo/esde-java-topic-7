package src;

import java.util.ArrayList;
import java.util.Arrays;

enum SegmentKind {
    Immutable,
    Mutable,
}

class Segment {
    SegmentKind kind;
    int value;

    Segment(SegmentKind kind, int value) {
        this.kind = kind;
        this.value = value;
    }

    public String toString() {
        return kind + ": " + value;
    }
}

public class Task5 {
    public static boolean solveSecretLock(int[] array) {
        if (array.length == 0) {
            return false;
        }

        var segments = new ArrayList<Segment>();

        int last = -1;
        var mutElementIndices = new ArrayList<Integer>();

        for (int i = 0; i < array.length; i++) {
            if (array[i] == 0) {
                continue;
            }

            int diff = i - last - 1;

            if (!segments.isEmpty() && diff == 0) {
                var lastSegment = segments.get(segments.size() - 1);
                if (lastSegment.kind.equals(SegmentKind.Immutable)) {
                    lastSegment.value = lastSegment.value * 10 + array[i];
                    last = i;
                    continue;
                }
            }

            if (diff > 0) {
                segments.add(
                        new Segment(
                                SegmentKind.Mutable,
                                Integer.parseInt("1".repeat(diff))
                        )
                );

                mutElementIndices.add(segments.size() - 1);
            }

            segments.add(new Segment(
                    SegmentKind.Immutable,
                    array[i]
            ));

            last = i;
        }

        if (array[array.length - 1] == 0) {
            for (int i = array.length - 1; i >= 0; i--) {
                if (array[i] != 0) {
                    segments.add(new Segment(
                            SegmentKind.Mutable,
                            Integer.parseInt("1".repeat(array.length - i - 1))
                    ));

                    mutElementIndices.add(segments.size() - 1);

                    break;
                }
            }
        }

        if (mutElementIndices.size() == 0) {
            Arrays.fill(array, 1);
            return true;
        }

        var currentMutIdx = mutElementIndices.size() - 1;
        while (true) {
            var value = generateResultNumber(segments);

            if (isRightValue(value)) {
                for (int i = 0; i < value.length(); i++) {
                    array[i] = (int) value.charAt(i) - 48;
                }

                return true;
            }

            try {
                var segment = segments.get(mutElementIndices.get(currentMutIdx));
                segment.value = nextValue(segment.value);

                var lastIndex = mutElementIndices.size() - 1;
                if (currentMutIdx != lastIndex) {
                    for (int i = currentMutIdx + 1; i <= lastIndex; i++) {
                        var s = segments.get(mutElementIndices.get(i));
                        s.value /= 6;
                    }

                    currentMutIdx = lastIndex;
                }
            } catch (Exception e) {
                if (currentMutIdx == 0) {
                    break;
                }

                currentMutIdx -= 1;
            }
        }

        return false;
    }

    private static int nextValue(int n) throws Exception {
        int output;

        int a = n % 10;
        if (a == 6) {
            if (n / 10 == 0) {
                throw new Exception("end");
            }

            output = nextValue(n / 10) * 10;
        } else {
            output = n + 1;
        }

        return output;
    }

    private static String generateResultNumber(ArrayList<Segment> segments) {
        StringBuilder output = new StringBuilder();

        for (Segment segment : segments) {
            output.append(segment.value);
        }

        return output.toString();
    }

    private static boolean isRightValue(String value) {
        var length = value.length();
        var number = Long.parseLong(value);

        for (int i = 0; i < length - 2; i++) {
            var threeDigits = (int) (number % (long) (Math.pow(10, i + 3)) / (long) Math.pow(10, i));

            if (sumOfDigits(threeDigits) != 10) {
                return false;
            }
        }

        return true;
    }

    private static int sumOfDigits(int value) {
        int sum = 0;

        while (value != 0) {
            sum += value % 10;
            value /= 10;
        }

        return sum;
    }
}