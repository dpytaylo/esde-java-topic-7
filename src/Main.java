package src;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        System.out.println("1. " + Task1.areCoprime(2, 2, 4));

        System.out.println("2. " + Task2.sumAsNumbers("9999999999999999999999912345", "67890"));

        System.out.println("3. Values:");
        Task3.findAllArmstrongNumbers(2000);

        System.out.println("4. " + Task4.determinate(new int[] {1, 2, 3, 4, 5, 6, 7}, 2, 5));

        // var array = new int[] {0, 1, 0, 0, 0, 3, 0, 0, 0, 0};
        // {6, 1, 3, 6, 1, 3, 6, 1, 3, 6};

        var array = new int[] {5, 4, 0, 0, 0, 0, 0, 0, 0, 0};
        // [5, 4, 1, 5, 4, 1, 5, 4, 1, 5]

        if (Task5.solveSecretLock(array)) {
            System.out.println("5.1. " + Arrays.toString(array));
        }
        else {
            System.out.println("5.1. Failed to solve secret lock!");
        }

        // var array2 = new int[] {0, 1, 0, 0, 0, 3, 0, 0, 0, 0};
        // {6, 1, 3, 6, 1, 3, 6, 1, 3, 6};

        var array2 = new int[] {5, 4, 0, 0, 0, 0, 0, 0, 0, 0};
        // [5, 4, 1, 5, 4, 1, 5, 4, 1, 5]

        if (ImprovedTask5.solveSecretLock(array2)) {
            System.out.println("5.2. " + Arrays.toString(array2));
        }
        else {
            System.out.println("5.2. Failed to solve secret lock!");
        }
    }
}