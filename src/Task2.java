package src;

public class Task2 {
    public static String sumAsNumbers(String a, String b) {
        var min = Integer.min(a.length(), b.length());
        var max = Integer.max(a.length(), b.length());
        StringBuilder result = new StringBuilder();

        int overflow = 0;
        for (int i = 0; i < min; i++) {
            int sum = (a.charAt(a.length() - i - 1) - '0') + (b.charAt(b.length() - i - 1) - '0') + overflow;
            result.insert(0, sum % 10);
            overflow = sum / 10;
        }

        for (int i = min; i < max; i++) {
            int sum = a.charAt(a.length() - i - 1) - '0' + overflow;
            result.insert(0, sum % 10);
            overflow = sum / 10;
        }

        return result.toString();
    }
}
