package src;

public class Task3 {
    public static void findAllArmstrongNumbers(int k) {
        for (int i = 1; i <= k; i++) {
            if (isArmstrongNumber(i)) {
                System.out.println("\t" + i);
            }
        }
    }

    private static boolean isArmstrongNumber(int value) {
        int temp = value;
        int sum = 0;

        var length = Integer.toString(value).length();
        while (temp != 0) {
            sum += Math.pow(temp % 10, length);
            temp /= 10;
        }

        return sum == value;
    }
}
