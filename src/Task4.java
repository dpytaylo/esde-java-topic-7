package src;

public class Task4 {
    public static int determinate(int[] D, int from, int to) {
        int sum = 0;
        for (int i = from; i <= to; i++) {
            sum += D[i];
        }

        return sum;
    }
}
