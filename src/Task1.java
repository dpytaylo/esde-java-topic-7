package src;

public class Task1 {
    public static boolean areCoprime(int a, int b, int c) {
        return gcd(a, b, c) == 1;
    }

    private static int gcd(int a, int b, int c) {
        int min = Integer.min(Integer.min(a, b), c);

        for (int i = min; i > 0; i--) {
            if (a % i == 0 && b % i == 0 && c % i == 0) {
                return i;
            }
        }

        return -1;
    }
}